<?php

/**
 * Funcion que retorna todas las razas de perro
 */
function getRazas()
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://dog.ceo/api/breeds/list/all");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    return $respuesta = curl_exec($ch);
    curl_close($ch);
}


/**
 * Funcion que retorna un link de una imagen .jpg de la raza o sub-raza elegida
 * @param string raza es la raza de perro.
 * @param string subraza subraza es la subraza exacta del perro, obtiene la primera imagen asociada.
 * Si el valor es 'random' entregar una imagen aleatoria del perro.
 * 
 */
function getRazasImgs($raza, $subraza)
{
    $ch = curl_init();
    if($subraza == 'random'){
        //obtengo solo 1 imagen random
        $url = "https://dog.ceo/api/breed/$raza/images/random";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($ch);
        $respuesta = json_decode($res,true);
        $imagen = $respuesta['message'];
    }else{
        //obtengo todas las imagenes asociadas a la raza y si tiene, subrazas
        $url = "https://dog.ceo/api/breed/$raza/images";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($ch);
        $respuesta = json_decode($res,true);
        $imagenes = $respuesta['message'];
        $imagen = '';
        foreach($imagenes as $img){
            if(strpos($img, $subraza) !== false){
                $imagen = $img;
                break;
            }else{
                continue;
            }
            
        }
    }

    curl_close($ch);
    return $imagen;
}
