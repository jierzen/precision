<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="estilo.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300&display=swap" rel="stylesheet">
    <title>PRECISION - Dog API</title>
    <?php 
        include('./getRazas.php');
        $razas = json_decode(getRazas(), true);
    ?>
</head>
<body class=".bg-secondary">
    <main>
        <div class="container">
            <div class="row">
                <h1 class="text-white">Precision - Dog API</h1>
                <h4 class="text-white"><strong>Postulante:</strong> Jorge Espinoza Ramirez</h4><br>
                <br>
                <p class="lead text-white">API utilizada: <a href="https://dog.ceo/dog-api">Dog Ceo API</a></p>
                <p class="lead text-white"><a href="https://bitbucket.org/jierzen/precision/src/main">Repo BitBucket</a></p>
                <p class="lead text-white">Plazo máximo de entrega:<strong> Martes 21 de Diciembre</strong> </p>
                <div class="col">
                <?php 
                $aux = 0;
                foreach($razas['message'] as $raza => $subraza){
                    $aux += 1;
                    ?>
                        <div class="accordion" id="accordion_<?= $aux ?>">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="heading_<?= $aux ?>">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse_<?= $aux ?>" aria-expanded="false" aria-controls="collapse_<?= $aux ?>">
                                        <?php print_r($raza) ?>
                                    </button>
                                </h2>
                                <div id="collapse_<?= $aux ?>" class="accordion-collapse collapse" aria-labelledby="heading_<?= $aux ?>" data-bs-parent="#accordion_<?= $aux ?>">
                    <?php
                    //si NO tiene sub razas
                    if(!$subraza){
                        $imagen = getRazasImgs($raza, 'random');
                        ?>
                            <div class="accordion-body">
                                <a href="<?php echo $imagen ?>"><img src="<?php echo $imagen ?>" id="gradient" width="300" height="300"></a><br>
                                <strong><a href="<?php echo $imagen ?>"><?php print_r($raza) ?></a></strong>
                            </div>
                        <?php

                    //si tiene sub razas
                    }else
                        {
                        ?> <ul class="grid-list"> <?php
                        foreach($subraza as $sub){
                            $imagenSub = getRazasImgs($raza, $sub);
                            ?>
                                <li>
                                    <div class="accordion-body">
                                        <a href="<?php echo $imagenSub ?>"><img src="<?php echo $imagenSub ?>" id="gradient" width="300" height="300"></a><br>
                                        <strong><a href="<?php echo $imagenSub ?>"><?php print_r($raza.' - '.$sub) ?></a></strong>
                                    </div>
                                </li>
                            <?php
                        }
                        ?> </ul> <?php
                    }
                    ?>
                    </div>
                        </div>
                    </div>
                    <?php
                } ?>
                </div>
            </div>
        </div>
    </main>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>

</html>